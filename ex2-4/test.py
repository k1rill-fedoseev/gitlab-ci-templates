import unittest
from utils import fib

class TestFib(unittest.TestCase):
    
    def test_first_values(self):
        self.assertEqual(fib(0), 1)
        self.assertEqual(fib(1), 1)

    def test_third_value(self):
        self.assertEqual(fib(2), 2)

    def test_tenth(self):
        self.assertEqual(fib(9), 55)

if __name__ == '__main__':
    unittest.main()